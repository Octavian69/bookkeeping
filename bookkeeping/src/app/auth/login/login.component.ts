import { Component, OnInit, AfterViewInit, HostBinding } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { UsersService } from '../../services/users.service';
import { User } from '../../shared/models/user.model';
import { Message } from '../../shared/models/message.model';
import { AuthService } from '../../services/auth.service';
import { fadeStateTrigger } from '../../shared/animations/fade.animation';
import { Title, Meta } from '@angular/platform-browser';


@Component({
  selector: 'office-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [fadeStateTrigger]
})
export class LoginComponent implements OnInit {

  form: FormGroup;
  message: Message;
 
  constructor(
    private usersService: UsersService,
    private authService: AuthService,
    private router: Router,
    private activeRouter: ActivatedRoute,
    private title: Title,
    private meta: Meta
  ) { 
    title.setTitle('Вход в систему');
    meta.addTags([
      {name: 'keywords', content: 'логин,вход,система'},
      {name: 'description', content: 'Страница для входа в систему.'},
    ])
  }


  ngOnInit() {
     this.message = new Message('', 'danger');

     this.form = new FormGroup({
      'email': new FormControl(null, [Validators.required, Validators.email]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6)]),

    })

    this.activeRouter.queryParams.subscribe((params: Params) => {
        if(params.nowCanLogin) {
          this.showMessage('Теперь вы можете войти в систему', 'success');
        } else if (params.accessDenied) {
          this.showMessage('Для входа в систему зарегистрируйтесь,как пользователь.', 'warning')
        }
    })
  }

  private showMessage(text: string,  type: string = 'danger') {
      this.message = new Message(text, type);

       setTimeout(()=> {
          this.message['text'] = '';
      }, 5000)
  }
  
  onSubmit() {
    const formData = this.form.value;

    this.usersService.getUserByEmail(formData.email).subscribe((user: User) => {
        if(user) {
          if(user.password === formData.password) {
              window.localStorage.setItem('user', JSON.stringify(user));
              this.authService.login();
              this.router.navigate(['system', 'bill'])

          } else {
            this.showMessage('Неверный пароль.')
          }

        } else {
          this.showMessage('Такой пользователь не зарегистрирован.')
        }
    })
  }
}

