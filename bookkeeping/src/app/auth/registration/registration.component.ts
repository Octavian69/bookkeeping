import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { UsersService } from '../../services/users.service';
import { User } from '../../shared/models/user.model';
import { Title } from '@angular/platform-browser';


@Component({
  selector: 'office-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  form: FormGroup;

  constructor(
    private userService: UsersService,
    private router: Router,
    private title: Title
  ) {
    title.setTitle('Регистрация');
   }

  ngOnInit() {
    this.form = new FormGroup({
      'email' : new FormControl(null, [Validators.required, Validators.email], [this.forbiddenEmails]),
      'password': new FormControl(null, [Validators.required, Validators.minLength(6)]),
      'name': new FormControl(null, [Validators.required]),
      'agree': new FormControl(false, [Validators.requiredTrue]),
    })
  }
  
  onSubmit() {
    const {email, password, name} = this.form.value;

    this.userService.createNewUser({email, password, name}).subscribe((data: User)=> {
          this.router.navigate(['/login'], {
            queryParams: {
              nowCanLogin: true
            }
          })
    })
  }

  forbiddenEmails = (control: FormControl): Promise<any> => {
      return new Promise((resolve, reject)=> {
        setTimeout(()=> {
            this.userService.getUserByEmail(control.value).subscribe((user: User)=> {
              user ? resolve({forbiddenEmail: true}) : resolve(null);
          })
        }, 3000)
      })
  }
}


// frobiddenEmails