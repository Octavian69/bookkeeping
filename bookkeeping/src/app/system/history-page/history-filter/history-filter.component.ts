import { Component, OnInit, Output, Input,EventEmitter } from '@angular/core';
import { Category } from '../../shared/models/category.model';
import { RecordsEvent } from '../../shared/models/event.model';

@Component({
  selector: 'office-history-filter',
  templateUrl: './history-filter.component.html',
  styleUrls: ['./history-filter.component.scss']
})
export class HistoryFilterComponent implements OnInit {

  selectedPeriod: string = 'd';
  selectedType: string[] = [];
  selectedCategories: number[] = [];

  @Output() onFilterCancel: any = new EventEmitter();
  @Output() applyFilter = new EventEmitter<{period: string, types: string[], categories: number[]}>();
  @Input() categories: Category[];


  timePeriod: {type: string, label: string}[] = [
    {type: 'd', label: 'День'},
    {type: 'w', label: 'Неделя'},
    {type: 'M', label: 'Месяц'}
  ];

  typeEvents: {type: string, label: string}[] = [
    {type: 'income', label:'Доход'},
    {type: 'outcome', label:'Расход'}
  ]


  constructor() { }

  ngOnInit() {
  }

  filterClose(): void {
    this.onFilterCancel.emit();
  }

  onFilterApply(): void {
      this.applyFilter.emit({
        period: this.selectedPeriod,
        types: this.selectedType,
        categories: this.selectedCategories,
      })

      this.selectedPeriod = '';
      this.selectedType = [];
      this.selectedCategories = [];
  }

  calculateInputParams(field, checked, value) {
    if(checked) {
      this[field].includes(value) ? null : this[field].push(value)
    } else {
      this[field] = this[field].filter(item => item !== value);
    }
  }

  handleChangeType({ checked, value }) {
    this.calculateInputParams('selectedType', checked, value);
    console.log(this.selectedType)
  }

  handleChangeCategory({ checked, value }) {
    this.calculateInputParams('selectedCategories', checked, value);
    console.log(this.selectedCategories)
  }

}

