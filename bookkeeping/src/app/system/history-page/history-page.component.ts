import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import * as moment from 'moment';

import { CategoriesService } from '../shared/services/catregories.service';
import { RecordEventService } from '../shared/services/events.service';
import { Category } from '../shared/models/category.model';
import { RecordsEvent } from '../shared/models/event.model';

@Component({
  selector: 'office-history-page',
  templateUrl: './history-page.component.html',
  styleUrls: ['./history-page.component.scss']
})
export class HistoryPageComponent implements OnInit {

  isLoaded: boolean = false;
  isFilterVisible: boolean = false;
  historySub: Subscription;

  categories: Category[] = [];
  events: RecordsEvent[] = [];
  filteredEvents: RecordsEvent[]  = [];
  charData: {name: string, value: number}[] = [];

  constructor(
    private categoriesService: CategoriesService,
    private recordEventService: RecordEventService
  ) { }

  ngOnInit() {
   this.historySub = Observable.combineLatest(
      this.categoriesService.getCategories(),
      this.recordEventService.getCatEvent()
    ).subscribe((data: [Category[], RecordsEvent[]])=> {
      this.categories = data[0];
      this.events = data[1];

      this.setOriginalEvents(); 
      this.calculateChartData();
      
       this.isLoaded = true;
    })
    
  }

  setOriginalEvents(): void {
    this.filteredEvents = this.events;
  }

  calculateChartData(): void {
      this.charData = [];

      this.categories.forEach(item => {
        const calcEvent = this.filteredEvents.filter(ev => item.id === ev.category && ev.type === 'outcome');
        const value = calcEvent.reduce((a, b) => {
           return  a + b.amount
        }, 0)
        this.charData.push( { name: item.name, value  } );
      })
  }

  ngOnDestroy(): void {
    if(this.historySub) this.historySub.unsubscribe();
  }

  private toogleFilterVisibility(visible: boolean) {
    this.isFilterVisible = visible;
  }

  openFilter(): void {
    this.toogleFilterVisibility(true);
  }
  
  onFilterCancel(): void {
    this.toogleFilterVisibility(false);
    this.setOriginalEvents();
    this.calculateChartData();
  }

  applyFilter(filterData) {

    const startDate = moment().startOf(filterData.period).startOf('d');
    const endDate = moment().endOf(filterData.period).endOf('d');
    
    this.toogleFilterVisibility(false);

    this.filteredEvents = this.filteredEvents
                .filter(item => item.type.includes(filterData.types))
                .filter(item => String(item.category).includes(filterData.categories))
                .filter(item => {
                  const momentDate = moment(item.date, 'DD.MM.YYYY HH.mm.ss');
                  return momentDate.isBetween(startDate, endDate)
                })

      this.calculateChartData();
  }
}
