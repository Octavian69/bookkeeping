import { Component, OnInit, Input } from '@angular/core';
import { Category } from '../../shared/models/category.model';
import { RecordsEvent } from '../../shared/models/event.model';
import { RecordEventService } from '../../shared/services/events.service';

@Component({
  selector: 'office-history-events',
  templateUrl: './history-events.component.html',
  styleUrls: ['./history-events.component.scss']
})
export class HistoryEventsComponent implements OnInit {

  searchPlaceholder: string = 'Поиск...';
  filterValue: string = '';

  @Input() events: RecordsEvent[];
  @Input() categories: Category[];

  constructor(
    private eventServices: RecordEventService
  ) { }

  ngOnInit() {
    this.events.forEach(item => {
      item.name = this.categories.find(category => item.category === category.id).name;
    })

  }

  getEventClass(ev: RecordsEvent) {
    return {
      "label" : true,
      "label-danger" : ev.type === 'outcome',
      "label-success": ev.type === 'income'
    }
  }

  changeCriteria(field): void {
    this.searchPlaceholder = field;
  }


}
