import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { RecordEventService } from '../../shared/services/events.service';
import { CategoriesService } from '../../shared/services/catregories.service';
import { RecordsEvent } from '../../shared/models/event.model';
import { Category } from '../../shared/models/category.model';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'office-history-details',
  templateUrl: './history-details.component.html',
  styleUrls: ['./history-details.component.scss']
})
export class HistoryDetailsComponent implements OnInit, OnDestroy {
  
  infoSub: Subscription;

  event: RecordsEvent;
  category: Category;

  isLoaded: boolean = false;
  

  constructor(
    private activeRoute: ActivatedRoute,
    private eventsService: RecordEventService,
    private categoriesService: CategoriesService
  ) { }

  ngOnInit() {
     this.infoSub = this.activeRoute.params
        .mergeMap((data: { id: string }) => this.eventsService.getEventById(data.id))
        .mergeMap((event: RecordsEvent) => {
          this.event = event;
          return  this.categoriesService.getCategoryById(event.category);
        })
        .subscribe((category: Category) => {
          this.category = category;

          this.isLoaded = true;
        })
  }

  ngOnDestroy(): void {
    if(this.infoSub) this.infoSub.unsubscribe();
  }

}
