import { Component, OnInit, Input } from '@angular/core';
import { CategoriesService } from '../../shared/services/catregories.service';
import { RecordEventService } from '../../shared/services/events.service';

@Component({
  selector: 'office-history-chart',
  templateUrl: './history-chart.component.html',
  styleUrls: ['./history-chart.component.scss']
})
export class HistoryChartComponent implements OnInit {

  @Input('charData') charData;

  view: number[] = [645, 455];
  data: {name: string, value: number}[] = [
    {
       name: 'Germany',
       value: 8940000   
    },
    {
      name: 'USA',
      value: 5000000 
    },
    {
      name: 'France',
      value: 7200000  
    }
  ]

  constructor(
    private categoriesService: CategoriesService,
    private recordEventService: RecordEventService
  ) { }

  ngOnInit() {
  }

}
