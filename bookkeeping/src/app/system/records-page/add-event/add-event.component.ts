import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';
import * as moment from 'moment';

import { Category } from '../../shared/models/category.model';
import { RecordsEvent } from '../../shared/models/event.model';
import { RecordEventService } from '../../shared/services/events.service';
import { BillService } from '../../shared/services/bill.service';
import { Bill } from '../../shared/models/bill.model';
import { Message } from '../../../shared/models/message.model';

@Component({
  selector: 'office-add-event',
  templateUrl: './add-event.component.html',
  styleUrls: ['./add-event.component.scss']
})
export class AddEventComponent implements OnInit, OnDestroy {

  @Input() categories: Category[] = [];

  types: {label: string, type: string}[] = [
    {label: 'Доход', type: 'income'},
    {label: 'Расход', type: 'outcome'}
  ]

  message: Message;
  billUpdateSub: Subscription;
  getBillSub: Subscription;

  constructor(
    private recordEvent: RecordEventService,
    private billService: BillService
  ) { }

  ngOnInit() {
    this.message = new Message('', '');
  }

  private showMessage(text, type) {
    this.message['text'] = text;
    this.message['type'] = type;
    
    if(type !== 'danger') {
        setTimeout(()=> {
          this.message['text'] = '';
        },3000)
    }
  }

  onSubmit(form: NgForm) {
    let { amount, category, description, type } = form.value;
    let date = moment().format('DD.MM.YYYY HH.mm.ss');

    const event = new RecordsEvent(
      type,
      amount,
      +category,
      date,
      description
    )

   this.getBillSub = this.billService.getBill().subscribe((bill: Bill)=> {
      let value = 0;
      if(bill.value < amount) {
        this.showMessage(`Недостаточно средств.Не хвататет ${amount - bill.value}pуб.`, 'danger');
        return false;

      } else {

        if(type === 'income') {
          value = bill.value + amount;
        } else {
          value = bill.value - amount;
        }

       this.billUpdateSub = this.billService.updateBill({value, currency: bill.currency})
          .mergeMap(x => this.recordEvent.addEvent(event))
          .subscribe((event: RecordsEvent) => {
              this.showMessage('Событие добавлено', 'success');
              form.setValue({
                category: 0,
                amount: 1,
                description: ' ',
                type: 'income'
              })
          })
      }
    })
    
  }

  ngOnDestroy(): void {
    if(this.getBillSub) this.getBillSub.unsubscribe();
    if(this.billUpdateSub) this.getBillSub.unsubscribe();
  }

}
