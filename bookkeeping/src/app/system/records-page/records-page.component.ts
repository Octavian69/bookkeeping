import { Component, OnInit } from '@angular/core';
import { Category } from '../shared/models/category.model';
import { CategoriesService } from '../shared/services/catregories.service';

@Component({
  selector: 'office-records-page',
  templateUrl: './records-page.component.html',
  styleUrls: ['./records-page.component.scss']
})
export class RecordsPageComponent implements OnInit {

  constructor(private categoryService: CategoriesService) { }

  categories: Category[] = [];

  isLoaded: boolean = false;
  
  ngOnInit() {
    this.categoryService.getCategories().subscribe((data: Category[]) => {
      this.categories = data;
      this.isLoaded = true;
    })
    

  }

  newCategoryAdded(category: Category): void {
    this.categories.push(category);
    console.log(category)
  }

}
