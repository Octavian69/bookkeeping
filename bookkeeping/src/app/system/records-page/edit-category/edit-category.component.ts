import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs/Subscription';

import { Category } from '../../shared/models/category.model';
import { CategoriesService } from '../../shared/services/catregories.service';
import { Message } from '../../../shared/models/message.model';

@Component({
  selector: 'office-edit-category',
  templateUrl: './edit-category.component.html',
  styleUrls: ['./edit-category.component.scss']
})
export class EditCategoryComponent implements OnInit, OnDestroy{

  @Input() categories: Category[];

  category: Category = { name: '',capacity: 0, id: 0 };
  currentCategoryId: number = 0;
  message: Message;
  updatingCategorySub: Subscription;

  constructor(private categoryService: CategoriesService) { }

  ngOnInit() {
    this.message = new Message('', 'success');
  }


  changeCategoryId() {
    if(+this.currentCategoryId === 0) return false;
    this.category = this.categories.find(category => category.id === +this.currentCategoryId);
  }

  onSubmit(form: NgForm) {
    let { name, capacity } = form.value;
    
    if(capacity < 0) capacity *= -1;

    const category: Category = { name, capacity };

    this.updatingCategorySub = this.categoryService.updatingCategory(this.currentCategoryId, category).subscribe((data)=> {
      this.categories[data['id']] = data;
      this.message['text'] = 'Категория отредактирована';
      setTimeout(()=> {
        this.message['text'] = '';
      }, 4000)
   })
  }

  ngOnDestroy(): void {
    if(this.updatingCategorySub) this.updatingCategorySub.unsubscribe();
  }
 
}
