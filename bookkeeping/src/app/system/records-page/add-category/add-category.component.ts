import { Component, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { CategoriesService } from '../../shared/services/catregories.service';
import { Category } from '../../shared/models/category.model';


@Component({
  selector: 'office-add-category',
  templateUrl: './add-category.component.html',
  styleUrls: ['./add-category.component.scss']
})
export class AddCategoryComponent implements OnDestroy {

  constructor(private categoryService: CategoriesService) { }

  addCategorySub: Subscription;

  @Output('onCategoryAdd') onCategoryAdd = new EventEmitter<Category>();

  onSubmit(form) {
    
    let { name, capacity } = form.value;

    if(capacity < 0) capacity *= -1;

    const category: Category = { name: name.trim(), capacity };

    this.addCategorySub = this.categoryService.addCategory(category)
      .subscribe((data: Category) => {
        form.form.patchValue({capacity: 1, name: ' '});
        this.onCategoryAdd.emit(data);
      })
  }

  ngOnDestroy(): void {
    if(this.addCategorySub) this.addCategorySub.unsubscribe();
  }

}
