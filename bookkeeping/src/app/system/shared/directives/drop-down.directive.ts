import { Directive, HostBinding, HostListener, ElementRef } from "@angular/core";


@Directive({
    selector: '[officeDrop]'
})
export class DropDownDirective {
    
    constructor(private element: ElementRef) {}

    // @HostBinding('class.open') isOpen = false;

    @HostListener('click') dropDown(): void {
        // this.isOpen = !this.isOpen;

        const { nativeElement } = this.element;
        nativeElement.classList.toggle('open');
    }
}