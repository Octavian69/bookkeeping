import { BaseApi } from "../../../shared/core/base-api";
import { Http } from "@angular/http";
import { Observable } from 'rxjs/Observable';

import { Category } from "../models/category.model";
import { Injectable } from "@angular/core";

@Injectable()
export class CategoriesService extends BaseApi {
    constructor(public http: Http) {
        super(http);
    }

    addCategory(category: Category): Observable<Category> {
        return this.post('categories', category);
    }

    getCategories(): Observable<Category[]> {
        return this.get('categories');
    }

    updatingCategory(id, category): Observable<Category> {
        return this.put(`categories/${id}`, category)
    }

    getCategoryById(id: number): Observable<Category> {
        return this.get(`categories/${id}`)
    }
}