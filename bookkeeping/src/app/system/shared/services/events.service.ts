import { Injectable } from "@angular/core";
import { Http } from "@angular/http";
import { Observable } from 'rxjs/Observable';

import { BaseApi } from "../../../shared/core/base-api";
import { RecordsEvent } from "../models/event.model";
import { CategoriesService } from "./catregories.service";
import { Category } from "../models/category.model";



@Injectable()
export class RecordEventService extends BaseApi {
    
    constructor(public http: Http) {
        super(http);
    }

    addEvent(event: RecordsEvent): Observable<RecordsEvent> {
        return this.post('events', event);
    }
    
    getCatEvent(): Observable<RecordsEvent[]> {
        return this.get('events');
    }

    getEventById(id: string): Observable<RecordsEvent> {
        return this.get(`events/${Number(id)}`);
    }
}