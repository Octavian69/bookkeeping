import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: 'officeFilter'
})
export class FilterPipe implements PipeTransform {
    transform(items: any[], value: string, filed: string) {
        if(!value || items.length === 0 || !filed ) return items;

        switch(filed) {
            case 'Сумма':
                
                return items.filter(item => String(item.amount).includes(value));

            case 'Дата':

                return items.filter(item => item.date.includes(value));

            case 'Категория':

                return items.filter(item => item.name.toLowerCase().includes(value.toLocaleLowerCase()));

            case 'Тип':

                if(value[0].toLocaleLowerCase() === 'д') {
                    
                    return items.filter(item => item.type === 'income');

                } else if (value[0].toLocaleLowerCase() === 'р'){

                    return items.filter(item => item.type === 'outcome');

                }

            default:
                return items;
        }
    }
}