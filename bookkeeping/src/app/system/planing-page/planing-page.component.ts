import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { BillService } from '../shared/services/bill.service';
import { CategoriesService } from '../shared/services/catregories.service';
import { RecordEventService } from '../shared/services/events.service';
import { Bill } from '../shared/models/bill.model';
import { Category } from '../shared/models/category.model';
import { RecordsEvent } from '../shared/models/event.model';

@Component({
  selector: 'office-planing-page',
  templateUrl: './planing-page.component.html',
  styleUrls: ['./planing-page.component.scss']
})
export class PlaningPageComponent implements OnInit, OnDestroy {

  isLoaded: boolean = false;
  planingSub: Subscription;

  bill: Bill;
  categories: Category[] = [];
  events: RecordsEvent[] = [];

  constructor(
    private billService: BillService,
    private categoriesService: CategoriesService,
    private eventCatService: RecordEventService
  ) { }

  ngOnInit() {
    this.planingSub = Observable.combineLatest(
      this.billService.getBill(),
      this.categoriesService.getCategories(),
      this.eventCatService.getCatEvent()
    ).subscribe((data: [Bill, Category[], RecordsEvent[]]) => {
        this.bill = data[0];
        this.categories = data[1];
        this.events = data[2];

        this.isLoaded = true;
    })
  }

  getCategoryPost(cat: Category): {value: number, percent: string, classColor: string} {
    let value = 0;

    this.events.forEach(item => {
        if(item.category === cat.id && item.type === 'outcome') value += item['amount'];
    })

    let percent = this.getPercentWidth(value, cat.capacity);
    let classColor = this.getCatColorClass(percent)

    return { value, percent: percent + '%', classColor };
  }

  getPercentWidth(value, limit): number {

    return (value * 100) / limit;
  }

  getCatColorClass(percent): string {
    return percent >= 100 ? 'danger' : percent >= 60 ? 'warning' : 'success';
  }

  ngOnDestroy(): void {
    if(this.planingSub) this.planingSub.unsubscribe();
  }

}
