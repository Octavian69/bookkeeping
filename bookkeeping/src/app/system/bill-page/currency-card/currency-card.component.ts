import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'office-currency-card',
  templateUrl: './currency-card.component.html',
  styleUrls: ['./currency-card.component.scss']
})
export class CurrencyCardComponent implements OnInit {

  constructor() { }

  @Input() currency: any;

  currencies: string[] = ['EUR', 'USD'];

  ngOnInit() {
   
  }

}
