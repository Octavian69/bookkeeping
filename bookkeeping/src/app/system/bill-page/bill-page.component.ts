import { Component, OnInit, OnDestroy } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';

import { BillService } from '../shared/services/bill.service';
import { Bill } from '../shared/models/bill.model';

@Component({
  selector: 'office-bill-page',
  templateUrl: './bill-page.component.html',
  styleUrls: ['./bill-page.component.scss']
})
export class BillPageComponent implements OnInit, OnDestroy {

  subscriptionInit: Subscription;
  subscriptionRefresh: Subscription;
  isLoaded: boolean = false;
  bill: Bill;
  currency: any;

  constructor(
    private billService: BillService
  ) { }

  ngOnInit() {
     this.subscriptionInit = Observable.combineLatest(
        this.billService.getBill(),
        this.billService.getCurrency()
      ).subscribe((data: [Bill, any])=> {
          this.bill = data[0];
          this.currency = data[1];
          this.isLoaded = true;
      })
  }

  ngOnDestroy() {
      this.subscriptionInit.unsubscribe();
      if(this.subscriptionRefresh) this.subscriptionRefresh.unsubscribe()
  }

  onRefresh(): void {
    this.isLoaded = false;
    this.subscriptionRefresh = this.billService.getCurrency().delay(4000).subscribe((data: any) => {
      this.currency = data;
      this.isLoaded = true;
    })
  }

}
