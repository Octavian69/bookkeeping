import { Component, OnInit, Input } from '@angular/core';
import { Bill } from '../../shared/models/bill.model';

@Component({
  selector: 'office-bill-card',
  templateUrl: './bill-card.component.html',
  styleUrls: ['./bill-card.component.scss']
})
export class BillCardComponent implements OnInit {

  constructor() { }

  @Input() bill: Bill;
  @Input() currency: any;
  euro: number;
  dollar: number;
  

  ngOnInit() {
    const { Valute } = this.currency;
    this.euro = this.bill.value / Valute['EUR']['Value'];
    this.dollar = this.bill.value / Valute['USD']['Value'];
  }

}
