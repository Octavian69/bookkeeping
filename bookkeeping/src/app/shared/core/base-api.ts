import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";


@Injectable()
export class BaseApi {
    
    constructor(public http: Http) {}

    baseUrl: string = 'http://localhost:3000/';


    public getUrl(url): string {
        return this.baseUrl + url;
    }


    public get(url: string): Observable<any> {
        return this.http.get(this.getUrl(url))
                .map((response: Response) => response.json());
    }


    public post(url: string, obj: any = {}): Observable<any> {
        return this.http.post(this.getUrl(url), obj)
                .map((response: Response) => response.json());
    }

    public put(url: string, obj: any = {}) {
        return this.http.put(this.getUrl(url), obj)
                .map((response: Response) => response.json());
    }
}   