import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { User } from "../shared/models/user.model";
import { BaseApi } from "../shared/core/base-api";

@Injectable()
export class UsersService extends BaseApi {
    constructor(public http: Http) {
        super(http);
    }


    getUserByEmail(email): Observable<any> {
        return this.get(`users?email=${email}`)
                    .map((user: User[]) => user[0] ? user[0] : undefined);
    }

    createNewUser(user: User): Observable<User> {
        return this.post('users', user);
    }
}