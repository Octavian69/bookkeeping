import { Observable } from 'rxjs/Observable';

export class AuthService {
    
    isAuthenticated: boolean = false;

    login(): void {
        this.isAuthenticated = true;
    }

    logout(): void {
        this.isAuthenticated = false;
        window.localStorage.clear()
    }

    isLoggedIn(): boolean {
        // return Observable.of(this.isAuthenticated).delay(3000);
        return this.isAuthenticated;
    }
    
}


// auth guard accessDenied