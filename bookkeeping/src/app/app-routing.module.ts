import { NgModule } from "@angular/core";
import { RouterModule, Routes, PreloadAllModules } from "@angular/router";
import { NotFoundPageComponent } from "./shared/components/not-found-page/not-found-page.component";
import { AuthGuard } from "./services/auth.guard";

const appRoutes: Routes = [
    { path: '', redirectTo: 'login', pathMatch: 'full'},
    { path: 'system', loadChildren: './system/system.module#SystemModule'},
    { path: '**', component: NotFoundPageComponent }
]

@NgModule({
    imports: [RouterModule.forRoot(appRoutes, {
        preloadingStrategy: PreloadAllModules
    })],
    exports: [RouterModule]
})
export class AppRoutingModule {}
